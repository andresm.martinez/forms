import { Component, OnInit } from '@angular/core';
import {  NgForm} from '@angular/forms';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styles: [`
    .ng-invalid.ng-touched{
      border = 1px solid red;
    }
  `]
})
export class TemplateComponent implements OnInit {


  usuario: Object = {
    nombre: "Andrés Mauricio",
    apellido: "Martinez Hincapié",
    correo: "andres.martinez@ucp.edu.co"
  }


  constructor() { }

  ngOnInit() {
  }

  guardar(forma: NgForm){
    console.log("Ésto es ngForm: ", forma);
    console.log("Solo los valores : ", forma.value);
    console.log("Con e objeto que se creó : ", this.usuario);
  }
}
